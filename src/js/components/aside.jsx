import React from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

const Aside = ({ isHeaderMenuOpen }) => (
  <aside className="toggle-elem">
    <nav className={isHeaderMenuOpen ? 'slideIn' : 'slideOut'}>
      <ul>
        <li>
          <NavLink exact to="/" activeClassName="selected">
            {'Home'}
          </NavLink>
        </li>
        <li>
          <NavLink to="/beef" activeClassName="selected">
            {'Beef'}
          </NavLink>
        </li>
        <li>
          <NavLink to="/fruits" activeClassName="selected">
            {'Fruits'}
          </NavLink>
        </li>
        <li>
          <NavLink to="/vegetables" activeClassName="selected">
            {'Vegetables'}
          </NavLink>
        </li>
      </ul>
    </nav>
  </aside>
);

Aside.propTypes = {
  isHeaderMenuOpen: PropTypes.bool.isRequired,
};
export default Aside;
