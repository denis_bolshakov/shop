import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import Beef from './beef';
import Cart from './cart';
import Fruits from './fruits';
import Home from './home';
import ProductInfo from './productinfo';
import Vegetables from './vegetables';
import PersonalAccount from './personalAccount';

const Main = ({
  actions,
  database,
  viewed,
  cart,
  items,
  itemsPerPage,
  calories,
  focusedAtCart,
  loading,
  error,
}) => (
  <main>
    <Switch>
      <Route
        exact
        path="/"
        render={() => (
          <Home
            actions={actions}
            database={database}
            viewed={viewed}
            cart={cart}
            items={items}
            itemsPerPage={itemsPerPage}
            loading={loading}
            error={error}
            url="/home"
          />
        )}
      />
      <Route path="/account" render={() => <PersonalAccount />} />
      <Route
        path="/beef"
        render={({ match }) => (
          <Beef
            actions={actions}
            database={database}
            viewed={viewed}
            cart={cart}
            items={items}
            loading={loading}
            error={error}
            itemsPerPage={itemsPerPage}
            url={match.url}
          />
        )}
      />
      <Route
        path="/fruits"
        render={({ match }) => (
          <Fruits
            actions={actions}
            database={database}
            viewed={viewed}
            cart={cart}
            items={items}
            loading={loading}
            error={error}
            itemsPerPage={itemsPerPage}
            url={match.url}
          />
        )}
      />
      <Route
        path="/vegetables"
        render={({ match }) => (
          <Vegetables
            actions={actions}
            database={database}
            viewed={viewed}
            cart={cart}
            items={items}
            loading={loading}
            error={error}
            itemsPerPage={itemsPerPage}
            url={match.url}
          />
        )}
      />
      <Route
        path="/cart"
        render={({ match }) => (
          <Cart
            key={calories}
            calories={calories}
            actions={actions}
            database={database}
            loading={loading}
            error={error}
            cart={cart}
            url={match.url}
            focusedAtCart={focusedAtCart}
          />
        )}
      />
      <Route
        path="/product/:id"
        render={({ match }) => (
          <ProductInfo
            loading={loading}
            error={error}
            database={database}
            actions={actions}
            url={match.url}
          />
        )}
      />
    </Switch>
  </main>
);

Main.propTypes = {
  actions: PropTypes.shape({}).isRequired,
  database: PropTypes.instanceOf(Array).isRequired,
  cart: PropTypes.instanceOf(Array).isRequired,
  calories: PropTypes.number.isRequired,
  items: PropTypes.number.isRequired,
  itemsPerPage: PropTypes.number.isRequired,
  viewed: PropTypes.instanceOf(Array).isRequired,
  focusedAtCart: PropTypes.number,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.bool.isRequired,
};
Main.defaultProps = {
  focusedAtCart: undefined,
};

export default Main;
