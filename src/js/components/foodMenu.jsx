import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { DropTarget } from 'react-dnd';
import FoodItem from './foodItem';

class FoodMenu extends React.Component {
  static propTypes = {
    connectDropTarget: PropTypes.func.isRequired,
    hovered: PropTypes.bool.isRequired,
    breakfast: PropTypes.instanceOf(Array).isRequired,
  };

  render() {
    const { connectDropTarget, hovered, breakfast } = this.props;
    const isOverStyle = hovered ? 'rgba(0,255,0,.4)' : 'rgba(255, 0,0 ,.2)';

    return connectDropTarget(
      <div>
        <h3>Завтрак</h3>
        <div
          className="flexLine"
          style={{ minHeight: '300px', background: isOverStyle }}
        >
          {breakfast.length > 0 ? (
            <FoodItem breakfast={breakfast} type="breakfast" />
          ) : (
            <p>Продукты не добавлены</p>
          )}
        </div>
      </div>
    );
  }
}
const spec = {
  drop(props, monitor) {
    const id = monitor.getItem().id;
    const category = 'breakfast';

    return {
      id,
      category,
    };
  },
};

const collect = (con, monitor) => ({
  connectDropTarget: con.dropTarget(),
  hovered: monitor.isOver(),
});

const mapStateToProps = state => ({
  breakfast: state.data.breakfast,
});

export default connect(
  mapStateToProps,
  null
)(DropTarget(['cart-item'], spec, collect)(FoodMenu));
