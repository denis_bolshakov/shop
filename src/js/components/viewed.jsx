import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import ViewedItem from './viewedItem';

export default class Viewed extends React.Component {
  static propTypes = {
    viewed: PropTypes.instanceOf(Array).isRequired,
  };

  constructor(props) {
    super(props);
    const { viewed } = this.props;
    this.state = {
      viewedLength: viewed.length,
      end: viewed.length,
      start: 0,
    };
  }

  componentDidMount() {
    const { viewed } = this.props;
    if (viewed.length > 5) {
      this.setState({
        start: viewed.length - 5,
      });
    }
  }

  handleBack = () => {
    this.setState(state => ({
      end: state.end - 1,
      start: state.start - 1,
    }));
  };

  handleForward = () => {
    this.setState(state => ({
      end: state.end + 1,
      start: state.start + 1,
    }));
  };

  loadProducts = () => {
    const { viewed } = this.props;
    const { viewedLength, start, end } = this.state;
    let currentItems;
    if (viewedLength >= 5) {
      currentItems = viewed
        .slice(start, end)
        .map(item => (
          <ViewedItem key={`product_${item.product_id}`} info={item} />
        ));
    } else {
      currentItems = viewed.map(item => (
        <ViewedItem key={`product_${item.product_id}`} info={item} />
      ));
    }
    return currentItems;
  };

  render() {
    const { viewedLength, start, end } = this.state;
    return (
      <Fragment>
        <h3>Просмотренные продукты:</h3>
        <div className="flexLine">{this.loadProducts()}</div>
        <button
          type="button"
          disabled={start === 0 ? 'disabled' : ''}
          onClick={() => this.handleBack()}
        >
          {'Назад'}
        </button>
        <button
          type="button"
          disabled={viewedLength === end ? 'disabled' : ''}
          onClick={() => this.handleForward()}
        >
          {'Вперед'}
        </button>
      </Fragment>
    );
  }
}
