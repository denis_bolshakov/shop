import React from 'react';

export default class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleSubmit = e => {
    e.preventDefault();
    fetch('/api/add', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ a: 1, b: 'Textual content' }),
    });
  };

  render() {
    return (
      <div>
        <h3>Форма</h3>

        <form onSubmit={this.handleSubmit}>
          <input type="text" name="name" />
          <input type="text" name="description" />
          <input type="submit" value="Submit" />
        </form>
      </div>
    );
  }
}
