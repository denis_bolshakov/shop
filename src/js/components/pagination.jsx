import React from 'react';
import PropTypes from 'prop-types';

export default class Pagination extends React.Component {
  static propTypes = {
    actions: PropTypes.shape({}).isRequired,
    url: PropTypes.string,
    items: PropTypes.number.isRequired,
    itemsPerPage: PropTypes.number.isRequired,
  };

  static defaultProps = {
    url: '/home',
  };

  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
    };
  }

  handleSetPage = (e, val) => {
    e.preventDefault();
    const { url, actions } = this.props;
    if (url) {
      actions.getData(url, val);
    } else {
      actions.getData(url, val);
    }

    this.setState({
      currentPage: val,
    });
  };

  loadPages = () => {
    const { items, itemsPerPage } = this.props;
    const { currentPage } = this.state;
    const pages = Math.ceil(items / itemsPerPage);
    const data = [];

    for (let i = 1; pages >= i; i++) {
      data.push(i);
    }
    const renderPages = data.map(currentValue => (
      <li key={currentValue}>
        <span
          role="button"
          tabIndex="0"
          data-index={currentValue}
          className={
            currentPage === currentValue ? 'pageAnchor active' : 'pageAnchor'
          }
          onClick={e => this.handleSetPage(e, currentValue)}
          onKeyPress={e => this.handleSetPage(e, currentValue)}
        >
          {currentValue}
        </span>
      </li>
    ));
    return <ul className="pagination">{renderPages}</ul>;
  };

  render() {
    return this.loadPages();
  }
}
