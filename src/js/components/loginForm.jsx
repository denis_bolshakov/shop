import React from 'react';
import PropTypes from 'prop-types';
import SignIn from './signIn';
import SignUp from './signUp';

class LoginForm extends React.Component {
  static propTypes = {
    actions: PropTypes.shape({}).isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      activeFormSignIn: true,
    };
  }

  toggleLoginMenu = () => {
    const { actions } = this.props;
    actions.toggleLoginMenu();
  };

  handleFormChange = type => {
    const { activeFormSignIn } = this.state;

    switch (type) {
      case 'in': {
        this.setState(() => ({
          activeFormSignIn: true,
        }));
        break;
      }
      case 'up': {
        this.setState(() => ({
          activeFormSignIn: false,
        }));
        break;
      }
    }
  };

  render() {
    const { actions, submitError, submittingLoginForm } = this.props;
    const { activeFormSignIn } = this.state;
    return (
      <div className="login-form-container">
        <div className="form-wrapper">
          <div>
            <button onClick={() => this.handleFormChange('in')}>SignIn</button>
            <button onClick={() => this.handleFormChange('up')}>SignUp</button>
            <span
              role="button"
              tabIndex="0"
              className="close-login-menu"
              onClick={() => this.toggleLoginMenu()}
              onKeyPress={() => this.toggleLoginMenu()}
            >
              <span>&times;</span>
            </span>
          </div>
          {activeFormSignIn ? (
            <SignIn
              actions={actions}
              submitError={submitError}
              submittingLoginForm={submittingLoginForm}
            />
          ) : (
            <SignUp actions={actions} submitError={submitError} />
          )}
        </div>
      </div>
    );
  }
}

export default LoginForm;
