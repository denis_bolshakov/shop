import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import LoadData from './loaddata';
import Pagination from './pagination';
import Viewed from './viewed';
import WithData from '../hocs/withData';
// import Form from './form';

const Home = ({
  database,
  items,
  url,
  itemsPerPage,
  actions,
  cart,
  loading,
  error,
  viewed,
}) => (
  <Fragment>
    <h2>Все продукты</h2>
    {items > 5 ? (
      <Pagination
        url={url}
        items={items}
        itemsPerPage={itemsPerPage}
        actions={actions}
      />
    ) : null}
    <LoadData
      error={error}
      loading={loading}
      database={database}
      cart={cart}
      actions={actions}
    />
    {viewed.length > 0 ? <Viewed viewed={viewed} /> : null}
  </Fragment>
);

Home.propTypes = {
  actions: PropTypes.shape({}).isRequired,
  database: PropTypes.instanceOf(Array).isRequired,
  cart: PropTypes.instanceOf(Array).isRequired,
  url: PropTypes.string.isRequired,
  items: PropTypes.number.isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.bool.isRequired,
  itemsPerPage: PropTypes.number.isRequired,
  viewed: PropTypes.instanceOf(Array).isRequired,
};

export default WithData(Home);
