﻿import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import HTML5Backend from 'react-dnd-html5-backend';
import { DragDropContext } from 'react-dnd';
import { connect } from 'react-redux';
import * as MyActions from '../actions/index';
import HeaderMenu from './headerMenu';
import Main from './main';
import Aside from './aside';
import LoginForm from './loginForm';

let App = ({
  actions,
  database,
  calories,
  viewed,
  cart,
  items,
  itemsPerPage,
  loading,
  error,
  focusedAtCart,
  isHeaderMenuOpen,
  isLoginMenuOpen,
  submitError,
  submittingLoginForm,
  user,
  location,
}) => (
  <Fragment>
    <HeaderMenu actions={actions} cartLength={cart.length} user={user} />
    <div className="container">
      <Aside isHeaderMenuOpen={isHeaderMenuOpen} />
      <Main
        actions={actions}
        database={database}
        viewed={viewed}
        cart={cart}
        items={items}
        itemsPerPage={itemsPerPage}
        calories={calories}
        loading={loading}
        error={error}
        focusedAtCart={focusedAtCart}
      />
    </div>
    {isLoginMenuOpen && (
      <LoginForm
        location={location}
        actions={actions}
        submitError={submitError}
        submittingLoginForm={submittingLoginForm}
      />
    )}
  </Fragment>
);

App.propTypes = {
  actions: PropTypes.shape({}).isRequired,
  database: PropTypes.instanceOf(Array).isRequired,
  cart: PropTypes.instanceOf(Array).isRequired,
  calories: PropTypes.number.isRequired,
  items: PropTypes.number.isRequired,
  itemsPerPage: PropTypes.number.isRequired,
  viewed: PropTypes.instanceOf(Array).isRequired,
  isHeaderMenuOpen: PropTypes.bool.isRequired,
  isLoginMenuOpen: PropTypes.bool.isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.bool.isRequired,
  focusedAtCart: PropTypes.number,
};

App.defaultProps = {
  focusedAtCart: undefined,
};

const mapStateToProps = state => ({
  focusedAtCart: state.data.focusedAtCart,
  database: state.data.database,
  cart: state.data.cart,
  calories: state.data.calories,
  items: state.data.items,
  itemsPerPage: state.data.itemsPerPage,
  viewed: state.data.viewed,
  loading: state.data.loading,
  error: state.data.error,
  isHeaderMenuOpen: state.panels.isHeaderMenuOpen,
  isLoginMenuOpen: state.panels.isLoginMenuOpen,
  submitError: state.users.error,
  submittingLoginForm: state.users.submitting,
  user: state.users.user,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(MyActions, dispatch),
});

App = DragDropContext(HTML5Backend)(App);

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(App)
);
