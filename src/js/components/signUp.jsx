import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';

const validate = ({ email, password, password2 }) => {
  const errors = {};

  if (!email) {
    errors.email = 'Required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) {
    errors.email = 'Invalid email address';
  }
  if (!password) {
    errors.password = 'Required';
  } else if (password.length < 8) errors.password = 'to short password';

  if (!password2) {
    errors.password2 = 'Required';
  } else if (password !== password2) {
    errors.password2 = 'passwords should be equal';
  }
  return errors;
};

const renderField = ({ input, label, type, meta: { touched, error } }) => (
  <div>
    <label htmlFor={label}>
      {label}
      <input
        {...input}
        placeholder={label}
        type={type}
        id={label}
        className={touched && error ? 'error' : ''}
      />
      {touched && error && <p className="error">{error}</p>}
    </label>
  </div>
);

class SignUp extends React.Component {
  static propTypes = {
    actions: PropTypes.shape({}).isRequired,
    handleSubmit: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const { actions } = this.props;
    actions.clearLoginFormError();
  }

  toggleLoginMenu = () => {
    const { actions } = this.props;
    actions.toggleLoginMenu();
  };

  onSubmit = values => {
    const { actions } = this.props;
    actions.signUp(values);
  };

  handleClear = () => {
    const { actions, submitError } = this.props;
    if (submitError) {
      actions.clearLoginFormError();
    }
  };

  render() {
    const { handleSubmit, submitError, submittingLoginForm } = this.props;

    return (
      <Fragment>
        <h2>Sign Up</h2>
        <form
          onSubmit={handleSubmit(this.onSubmit)}
          onClick={() => this.handleClear()}
          className="login-form form-signup"
        >
          <Field
            name="email"
            type="email"
            component={renderField}
            label="Email"
          />
          <Field
            name="password"
            type="password"
            component={renderField}
            label="Password"
          />
          <Field
            name="password2"
            type="password"
            component={renderField}
            label="Password2"
          />
          {submittingLoginForm ? (
            <p>Submitting...</p>
          ) : (
            <button type="submit">Submit</button>
          )}
          <span style={{ color: 'red' }}>{submitError}</span>
        </form>
      </Fragment>
    );
  }
}

export default reduxForm({
  form: 'signUp',
  validate,
})(SignUp);
