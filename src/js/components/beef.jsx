import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Pagination from './pagination';
import LoadData from './loaddata';
import Viewed from './viewed';
import WithData from '../hocs/withData';

const Beef = ({
  database,
  items,
  url,
  itemsPerPage,
  loading,
  error,
  actions,
  cart,
  viewed,
}) => (
  <Fragment>
    <h2>Мясо</h2>
    {items > 5 ? (
      <Pagination
        url={url}
        items={items}
        itemsPerPage={itemsPerPage}
        actions={actions}
      />
    ) : null}
    <LoadData
      loading={loading}
      error={error}
      database={database}
      cart={cart}
      actions={actions}
    />
    {viewed.length > 0 ? <Viewed viewed={viewed} /> : null}
  </Fragment>
);

Beef.propTypes = {
  actions: PropTypes.shape({}).isRequired,
  database: PropTypes.instanceOf(Array).isRequired,
  cart: PropTypes.instanceOf(Array).isRequired,
  url: PropTypes.string.isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.bool.isRequired,
  items: PropTypes.number.isRequired,
  itemsPerPage: PropTypes.number.isRequired,
  viewed: PropTypes.instanceOf(Array).isRequired,
};

export default WithData(Beef);
