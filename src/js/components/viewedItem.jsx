import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const ViewedItem = ({
  info: { name, product_id: productId, image, title },
}) => (
  <div className="item">
    <p>
      <Link to={`product/${productId}`}>{name}</Link>
    </p>
    <img src={`src/img/${image}`} alt={title} />
  </div>
);

ViewedItem.propTypes = {
  info: PropTypes.shape({}).isRequired,
};
export default ViewedItem;
