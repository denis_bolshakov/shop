import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

const personalAccount = ({ actions, user }) => (
  <Fragment>
    <h2>Личный кабинет</h2>
  </Fragment>
);

personalAccount.propTypes = {
  actions: PropTypes.shape({}).isRequired,
};

export default personalAccount;
