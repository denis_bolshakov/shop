import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

export default class CartInfo extends React.Component {
  static propTypes = {
    cart: PropTypes.instanceOf(Array).isRequired,
    calories: PropTypes.number.isRequired,
  };

  constructor(props) {
    super(props);
    const { cart, calories } = this.props;
    this.state = {
      length: cart.length,
      calories,
    };
  }

  handleCalories = () => {
    let sum = 0;
    const { cart } = this.props;
    cart.forEach(currentValue => {
      sum += currentValue.calories;
    });
    return sum;
  };

  render() {
    const { length, calories } = this.state;
    return (
      <Fragment>
        <h3>Информация о продуктах</h3>
        <p>{`Всего продуктов добавлено - ${length}`}</p>
        <p>{`Всего каллорий - ${calories}`}</p>
      </Fragment>
    );
  }
}
