import React from 'react';
import PropTypes from 'prop-types';
import { DragSource } from 'react-dnd';

class CartItem extends React.Component {
  static propTypes = {
    actions: PropTypes.shape({}).isRequired,
    info: PropTypes.shape({}).isRequired,
    focusedAtCart: PropTypes.number,
    connectDragSource: PropTypes.func.isRequired,
    isDragging: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    focusedAtCart: undefined,
  };

  constructor(props) {
    super(props);
    const { info } = this.props;
    const { quantity, calories, proteins, fats, carbo } = info;
    this.state = {
      quantity,
      calories,
      proteins,
      fats,
      carbo,
    };
  }

  componentDidMount() {
    const { info, focusedAtCart } = this.props;

    if (info.product_id === focusedAtCart) {
      this.inputControl.focus();
    }
  }

  handleReduce = () => {
    const { quantity, calories, proteins, fats, carbo } = this.state;
    const { actions, info } = this.props;
    const currentValue = quantity;
    const newValue = currentValue - 50;

    if (newValue <= 0) return;
    actions.newValueAtCart({
      ...info,
      quantity: newValue,
      calories: (newValue * calories) / currentValue,
      proteins: (newValue * proteins) / currentValue,
      fats: (newValue * fats) / currentValue,
      carbo: (newValue * carbo) / currentValue,
    });
  };

  handleIncrease = () => {
    const { quantity, calories, proteins, fats, carbo } = this.state;
    const { actions, info } = this.props;
    const currentValue = quantity;
    const newValue = currentValue + 50;

    actions.newValueAtCart({
      ...info,
      quantity: newValue,
      calories: (newValue * calories) / currentValue,
      proteins: (newValue * proteins) / currentValue,
      fats: (newValue * fats) / currentValue,
      carbo: (newValue * carbo) / currentValue,
    });
  };

  handleChangeManual = e => {
    const { quantity, calories, proteins, fats, carbo } = this.state;
    const { actions, info } = this.props;
    const currentValue = quantity;
    const newValue = parseInt(e.target.value, 10);

    if (newValue <= 0 || newValue === currentValue) return;

    actions.newValueAtCart({
      ...info,
      quantity: newValue,
      calories: (newValue * calories) / currentValue,
      proteins: ((newValue * proteins) / currentValue).toFixed(2),
      fats: ((newValue * fats) / currentValue).toFixed(2),
      carbo: ((newValue * carbo) / currentValue).toFixed(2),
    });

    this.setState(() => ({
      focused: true,
    }));
  };

  handleRemove = () => {
    const { actions, info } = this.props;
    const { atCart, calories } = this.state;

    this.setState(() => ({
      atCart: !atCart,
    }));

    actions.removeFromCart(info.product_id, calories);
  };

  render() {
    const { info, connectDragSource, isDragging } = this.props;
    const { name, image, title } = info;
    const { quantity, calories, proteins, fats, carbo } = this.state;
    const draggStyle = isDragging ? 'rgba(0,255,0,.4)' : '';
    return connectDragSource(
      <div className="item" style={{ background: draggStyle }}>
        <span
          role="button"
          tabIndex="0"
          className="removeFromCart"
          onClick={() => this.handleRemove()}
          onKeyPress={() => this.handleRemove()}
        >
          <span>&times;</span>
        </span>
        <p>{name}</p>
        <img src={`src/img/${image}`} alt={title} />
        <p>Расчет на (грамм):</p>
        <div className="caloriesCounter">
          <button type="button" onClick={() => this.handleReduce()}>
            {'-'}
          </button>
          <input
            ref={inp => {
              this.inputControl = inp;
            }}
            onChange={this.handleChangeManual}
            type="text"
            value={quantity}
          />
          <button type="button" onClick={() => this.handleIncrease()}>
            {'+'}
          </button>
        </div>
        <ul>
          <li>{`Ккал: ${calories}`}</li>
          <li>{`Белки: ${proteins}`}</li>
          <li>{`Жиры: ${fats}`}</li>
          <li>{`Углеводы: ${carbo}`}</li>
        </ul>
      </div>
    );
  }
}

const spec = {
  beginDrag(props) {
    return {
      id: props.info.product_id,
    };
  },
  endDrag(props, monitor) {
    const id = monitor.getDropResult().id;
    const category = monitor.getDropResult().category;
    props.actions.addToFoodMenu(id, category);
  },
};

const collect = (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
  isDragging: monitor.isDragging(),
});

export default DragSource('cart-item', spec, collect)(CartItem);
