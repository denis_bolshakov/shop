import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default class Item extends React.Component {
  static propTypes = {
    actions: PropTypes.shape({}).isRequired,
    atCart: PropTypes.bool.isRequired,
    info: PropTypes.shape({}).isRequired,
  };

  constructor(props) {
    super(props);
    const { atCart, info } = this.props;
    const { quantity, calories, proteins, fats, carbo } = info;
    this.state = {
      atCart,
      quantity,
      calories,
      proteins,
      fats,
      carbo,
    };
  }

  handleReduce = () => {
    const { quantity, calories, proteins, fats, carbo } = this.state;
    const currentValue = quantity;
    const newValue = currentValue - 50;

    if (newValue <= 0) return;

    this.setState(() => ({
      quantity: newValue,
      calories: (newValue * calories) / currentValue,
      proteins: (newValue * proteins) / currentValue,
      fats: (newValue * fats) / currentValue,
      carbo: (newValue * carbo) / currentValue,
    }));
  };

  handleIncrease = () => {
    const { quantity, calories, proteins, fats, carbo } = this.state;
    const currentValue = quantity;
    const newValue = currentValue + 50;
    this.setState(() => ({
      quantity: newValue,
      calories: (newValue * calories) / currentValue,
      proteins: (newValue * proteins) / currentValue,
      fats: (newValue * fats) / currentValue,
      carbo: (newValue * carbo) / currentValue,
    }));
  };

  handleChangeManual = e => {
    const { quantity, calories, proteins, fats, carbo } = this.state;
    const currentValue = quantity;
    const newValue = parseInt(e.target.value, 10);

    if (newValue <= 0 || newValue === currentValue) return;

    this.setState({
      quantity: newValue,
      calories: (newValue * calories) / currentValue,
      proteins: (newValue * proteins) / currentValue,
      fats: (newValue * fats) / currentValue,
      carbo: (newValue * carbo) / currentValue,
    });
  };

  addToCart = () => {
    const { atCart, quantity, calories, proteins, fats, carbo } = this.state;
    const { actions, info } = this.props;

    this.setState(() => ({
      atCart: !atCart,
    }));

    actions.addToCart({
      ...info,
      quantity,
      calories,
      proteins,
      fats,
      carbo,
    });
  };

  handleRemove = () => {
    const { atCart, calories } = this.state;
    const { actions, info } = this.props;

    this.setState(() => ({
      atCart: !atCart,
    }));

    actions.removeFromCart(info.product_id, calories);
  };

  render() {
    const { info } = this.props;
    const { name, image, title, product_id: productId } = info;
    const { atCart, quantity, calories, proteins, fats, carbo } = this.state;
    return (
      <div className="item">
        {atCart ? (
          <span
            role="button"
            tabIndex="0"
            className="removeFromCart"
            onClick={() => this.handleRemove()}
            onKeyPress={() => this.handleRemove()}
          >
            <span>&times;</span>
          </span>
        ) : null}
        <p>
          <Link to={`product/${productId}`}>{name}</Link>
        </p>
        <img src={`src/img/${image}`} alt={title} />
        <p>Расчет на (грамм):</p>
        <div className="caloriesCounter">
          <button
            type="button"
            disabled={atCart ? 'disabled' : ''}
            onClick={() => this.handleReduce()}
          >
            {'-'}
          </button>
          <input
            disabled={atCart ? 'disabled' : ''}
            onChange={this.handleChangeManual}
            type="text"
            value={quantity}
          />
          <button
            type="button"
            disabled={atCart ? 'disabled' : ''}
            onClick={() => this.handleIncrease()}
          >
            {'+'}
          </button>
        </div>
        <ul>
          <li>{`Ккал: ${calories}`}</li>
          <li>{`Белки: ${proteins}`}</li>
          <li>{`Жиры: ${fats}`}</li>
          <li>{`Углеводы: ${carbo}`}</li>
        </ul>
        <button
          type="button"
          className="add"
          disabled={atCart ? 'disabled' : ''}
          onClick={() => this.addToCart()}
        >
          {atCart ? 'В корзине' : 'Добавить'}
        </button>
      </div>
    );
  }
}
