import React from 'react';
import PropTypes from 'prop-types';
import LoadData from './loaddata';
import WithData from '../hocs/withData';

const ProductInfo = ({ actions, error, loading, database }) => (
  <LoadData
    error={error}
    loading={loading}
    database={database}
    actions={actions}
    type="product"
  />
);

ProductInfo.propTypes = {
  actions: PropTypes.shape({}).isRequired,
  database: PropTypes.instanceOf(Array).isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.bool.isRequired,
};

export default WithData(ProductInfo);
