import React from 'react';
import PropTypes from 'prop-types';

class FoodItem extends React.Component {
  static propTypes = {
    type: PropTypes.string.isRequired,
    breakfast: PropTypes.instanceOf(Array).isRequired,
  };

  loadGoods = type => {
    switch (type) {
      case 'breakfast': {
        const { breakfast } = this.props;
        const items = breakfast.map(item => (
          <div className="item" key={item.product_id}>
            {item.name}
          </div>
        ));
        return items;
      }
      default:
    }
  };

  render() {
    const { type } = this.props;
    return <div>{this.loadGoods(type)}</div>;
  }
}

export default FoodItem;
