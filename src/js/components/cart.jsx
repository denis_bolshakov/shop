import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import CartInfo from './cartinfo';
import LoadData from './loaddata';
import FoodMenu from './foodMenu';

const Cart = ({ cart, calories, database, focusedAtCart, actions }) => (
  <Fragment>
    <h2>Корзина</h2>
    {cart.length > 0 ? (
      <Fragment>
        <CartInfo cart={cart} calories={calories} />
        <LoadData
          focusedAtCart={focusedAtCart}
          database={database}
          cart={cart}
          actions={actions}
          type="cart"
        />
        <FoodMenu />
      </Fragment>
    ) : (
      <p>Продукты не добавлены</p>
    )}
  </Fragment>
);

Cart.propTypes = {
  actions: PropTypes.shape({}).isRequired,
  database: PropTypes.instanceOf(Array).isRequired,
  cart: PropTypes.instanceOf(Array).isRequired,
  calories: PropTypes.number.isRequired,
  focusedAtCart: PropTypes.number,
};
Cart.defaultProps = {
  focusedAtCart: undefined,
};
export default Cart;
