import React from 'react';
import PropTypes from 'prop-types';
import Item from './item';
import CartItem from './cartitem';
import WithLoading from '../hocs/withLoading';
import WithError from '../hocs/withError';

class LoadData extends React.Component {
  static propTypes = {
    actions: PropTypes.shape({}).isRequired,
    database: PropTypes.instanceOf(Array).isRequired,
    cart: PropTypes.instanceOf(Array),
    type: PropTypes.string,
    focusedAtCart: PropTypes.number,
  };

  static defaultProps = {
    cart: [],
    type: '',
    focusedAtCart: undefined,
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  loadGoods = type => {
    switch (type) {
      case 'cart': {
        // load items at cart
        const { cart, actions, focusedAtCart } = this.props;
        const cartData = cart.map(item => (
          <CartItem
            focusedAtCart={focusedAtCart}
            key={item.product_id}
            info={item}
            actions={actions}
          />
        ));
        return cartData;
      }
      case 'product': {
        // load custom product
        const { database } = this.props;
        const product = database.map(item => (
          <div key={item.product_id}>
            <img src={`/src/img/${item.image}`} alt={item.title} />
            <p>{item.name}</p>
            <p>{item.description}</p>
            <p>{`Каллорийность на ${item.quantity} грамм`}</p>
            <ul>
              <li>{`Ккал: ${item.calories}`}</li>
              <li>{`Белки: ${item.proteins}`}</li>
              <li>{`Жиры: ${item.fats}`}</li>
              <li>{`Углеводы: ${item.carbo}`}</li>
            </ul>
          </div>
        ));
        return product;
      }

      default: {
        // load products
        const { database, cart, actions } = this.props;
        const data = database.map(databaseItem => {
          // check item at cart
          const result = cart.find(val => {
            if (val.product_id === databaseItem.product_id) {
              return val;
            }
            return false;
          });

          if (result) {
            // if item at cart load data from cart props
            return (
              <Item
                key={databaseItem.product_id}
                info={result}
                actions={actions}
                atCart
              />
            );
          }
          return (
            <Item
              key={databaseItem.product_id}
              info={databaseItem}
              actions={actions}
              atCart={false}
            />
          );
        });
        return data;
      }
    }
  };

  render() {
    const { type } = this.props;
    return <div className="flexLine">{this.loadGoods(type)}</div>;
  }
}

export default WithError(WithLoading(LoadData));
