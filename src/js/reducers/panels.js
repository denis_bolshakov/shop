import { TOGGLE_HEADER_MENU, TOGGLE_LOGIN_MENU } from '../constants';

export const initialState = {
  isHeaderMenuOpen: true,
  isLoginMenuOpen: false,
};

export default function panels(state = initialState, action) {
  switch (action.type) {
    case TOGGLE_HEADER_MENU: {
      return {
        ...state,
        isHeaderMenuOpen: !state.isHeaderMenuOpen,
      };
    }
    case TOGGLE_LOGIN_MENU: {
      return {
        ...state,
        isLoginMenuOpen: !state.isLoginMenuOpen,
      };
    }
    default:
      return state;
  }
}
