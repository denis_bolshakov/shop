import {
  ADD_VIEWED,
  ADD_TO_CART,
  DATA_SUCCESS,
  DATA_PENDING,
  DATA_ERROR,
  PRODUCT_SUCCESS,
  NEW_VALUE_AT_CART,
  REMOVE_FROM_CART,
  ADD_TO_BREAKFAST,
} from '../constants';

const initialState = {
  database: [],
  cart: [],
  breakfast: [],
  calories: 0,
  viewed: [],
  items: 0,
  itemsPerPage: 5,
  loading: false,
  error: false,
  focusedAtCart: undefined,
};

export default function data(state = initialState, action) {
  switch (action.type) {
    case ADD_TO_BREAKFAST:
      return {
        ...state,
        breakfast: [...state.breakfast, action.item],
      };
    case ADD_VIEWED: {
      let currentViewed = state.viewed;
      // if already in viewed delete from array
      currentViewed = currentViewed.filter(
        curElem => curElem.product_id !== action.database.product_id
      );

      if (currentViewed.length === 10) {
        // delete first at array
        currentViewed.shift();
        currentViewed = [...currentViewed, action.database];
      } else {
        currentViewed = [...currentViewed, action.database];
      }
      return {
        ...state,
        viewed: currentViewed,
      };
    }
    case PRODUCT_SUCCESS:
      return {
        ...state,
        database: action.database,
        items: action.database.length,
        loading: false,
        error: false,
      };
    case DATA_SUCCESS:
      return {
        ...state,
        database: action.database.data,
        itemsPerPage: action.database.itemsPerPage,
        items: action.database.items,
        loading: false,
        error: false,
      };
    case DATA_PENDING:
      return {
        ...state,
        loading: true,
      };
    case DATA_ERROR:
      return {
        ...state,
        error: true,
      };
    case ADD_TO_CART: {
      const newCalories = state.calories + action.product.calories;
      return {
        ...state,
        cart: [...state.cart, action.product],
        calories: newCalories,
      };
    }
    case REMOVE_FROM_CART: {
      const newCalories = state.calories - action.calories;
      const newCart = state.cart.filter(
        curElem => curElem.product_id !== action.id
      );
      return {
        ...state,
        cart: newCart,
        calories: newCalories,
      };
    }

    case NEW_VALUE_AT_CART: {
      let newCalories = state.calories;
      const currentItem = action.val.product_id;
      const newCart = state.cart.map(curItem => {
        if (curItem.product_id === currentItem) {
          newCalories = newCalories - curItem.calories + action.val.calories;
          return action.val;
        }
        return curItem;
      });

      return {
        ...state,
        cart: newCart,
        calories: newCalories,
        focusedAtCart: currentItem,
      };
    }
    default:
      return state;
  }
}
