import {
  LOGIN_FORM_SUBMITTING,
  LOGIN_FORM_SUCCESS,
  LOGIN_FORM_ERROR,
  CLEAR_LOGIN_FORM_ERROR,
} from '../constants';

export const initialState = {
  user: false,
  submitting: false,
  error: false,
};

export default function panels(state = initialState, action) {
  const { type, payload, error } = action;

  switch (type) {
    case LOGIN_FORM_SUBMITTING:
      return {
        ...state,
        submitting: true,
      };
    case LOGIN_FORM_SUCCESS:
      return {
        ...state,
        user: payload.user,
        submitting: false,
        error: false,
      };
    case LOGIN_FORM_ERROR:
      return {
        ...state,
        submitting: false,
        error: error.message,
      };
    case CLEAR_LOGIN_FORM_ERROR:
      return {
        ...state,
        error: false,
      };
    default:
      return state;
  }
}
