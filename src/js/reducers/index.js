import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';
import data from './data';
import panels from './panels';
import users from './users';

const rootReducer = combineReducers({
  data,
  panels,
  users,
  form: formReducer,
  router,
});

export default rootReducer;
