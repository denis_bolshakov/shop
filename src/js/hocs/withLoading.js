import React from 'react';

const WithLoading = function WithLoading(Component) {
  return function WihLoadingComponent({ loading, ...props }) {
    return loading ? <p>Loading...</p> : <Component {...props} />;
  };
};
export default WithLoading;
