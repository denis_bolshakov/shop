import React from 'react';

const withData = Component => {
  class WithData extends React.Component {
    componentDidMount() {
      // load data from db
      const { actions, url } = this.props;

      if (Component.name === 'ProductInfo') {
        actions.getProduct(url);
      } else {
        actions.getData(url, 1);
      }
    }

    render() {
      return <Component {...this.props} />;
    }
  }

  return WithData;
};

export default withData;
