import React from 'react';

const WithError = function WithError(Component) {
  return function WithErrorComponent({ error, ...props }) {
    if (!error) return <Component {...props} />;
    return <p>Something goes wrong, try to reload the page...</p>;
  };
};
export default WithError;
