export default ({ dispatch }) => next => action => {
  if (action.type === 'GET_PRODUCT') {
    fetch(`/api${action.id}`)
      .then(response => response.json())
      .then(data => next({ ...action, database: data }))
      .then(data =>
        dispatch({ type: 'ADD_VIEWED', database: data.database[0] })
      );
    return false;
  }
  return next(action);
};
