export default store => next => action => {
    if (action.type=="GET_DATA") {
       fetch('/api'+ action.category + "?page=" + action.page)
          .then(response => response.json())
          .then((data) => next({ ...action, database: data}))
     } else return next(action)
}