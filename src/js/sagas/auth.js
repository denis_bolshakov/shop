import { push } from 'react-router-redux';
import { takeEvery, take, put, call } from 'redux-saga/effects';
import firebase from 'firebase';

import {
  SIGN_IN_REQUEST,
  SIGN_UP_REQUEST,
  LOGIN_FORM_SUBMITTING,
  LOGIN_FORM_SUCCESS,
  LOGIN_FORM_ERROR,
  TOGGLE_LOGIN_MENU,
  REDIRECT_TO_PERSONAL_ACCOUNT,
} from '../constants';

export function* signUpSaga() {
  while (true) {
    const action = yield take(SIGN_UP_REQUEST);
    yield put({ type: LOGIN_FORM_SUBMITTING });
    try {
      const user = yield call(() =>
        firebase
          .auth()
          .createUserWithEmailAndPassword(
            action.values.email,
            action.values.password
          )
      );
      yield put({ type: LOGIN_FORM_SUCCESS, payload: user });
      yield put({ type: TOGGLE_LOGIN_MENU });
      yield put(push('/account'));
    } catch (error) {
      yield put({ type: LOGIN_FORM_ERROR, error });
    }
  }
}

export function* signInSaga() {
  while (true) {
    const action = yield take(SIGN_IN_REQUEST);
    yield put({ type: LOGIN_FORM_SUBMITTING });
    try {
      const user = yield call(() =>
        firebase
          .auth()
          .signInWithEmailAndPassword(
            action.values.email,
            action.values.password
          )
      );
      yield put({ type: LOGIN_FORM_SUCCESS, payload: user });
      yield put({ type: TOGGLE_LOGIN_MENU });
      yield put(push('/account'));
    } catch (error) {
      yield put({ type: LOGIN_FORM_ERROR, error });
    }
  }
}

export function* redirectToPersonalAccount() {
  yield takeEvery(REDIRECT_TO_PERSONAL_ACCOUNT, function* test() {
    yield put(push('/account'));
  });
}
