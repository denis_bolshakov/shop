import { all } from 'redux-saga/effects';
import { signInSaga, signUpSaga, redirectToPersonalAccount } from './auth';
import { getData, getProduct } from './data';
import { foodMenuSaga } from './foodmenu';

export default function* rootSaga() {
  yield all([
    getData(),
    getProduct(),
    signInSaga(),
    signUpSaga(),
    redirectToPersonalAccount(),
    foodMenuSaga(),
  ]);
}
