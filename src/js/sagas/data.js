import { takeEvery, put, call } from 'redux-saga/effects';

import {
  PRODUCT_SUCCESS,
  DATA_SUCCESS,
  DATA_PENDING,
  DATA_ERROR,
  GET_DATA,
  GET_PRODUCT,
} from '../constants';

export function* getData() {
  yield takeEvery(GET_DATA, function* test({ category, page }) {
    yield put({ type: DATA_PENDING });

    try {
      const data = yield call(() =>
        fetch(`/api${category}?page=${page}`).then(res => res.json())
      );

      yield put({ type: DATA_SUCCESS, database: data });
    } catch (error) {
      yield put({ type: DATA_ERROR, error });
    }
  });
}

export function* getProduct() {
  yield takeEvery(GET_PRODUCT, function* test({ id }) {
    yield put({ type: DATA_PENDING });
    try {
      const data = yield call(() => fetch(`/api${id}`).then(res => res.json()));
      yield put({ type: PRODUCT_SUCCESS, database: data });
    } catch (error) {
      yield put({ type: DATA_ERROR, error });
    }
  });
}
