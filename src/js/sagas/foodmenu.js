import { take, put, select } from 'redux-saga/effects';

import { ADD_TO_FOOD_MENU, ADD_TO_BREAKFAST } from '../constants';

export function* foodMenuSaga() {
  while (true) {
    const action = yield take(ADD_TO_FOOD_MENU);
    const state = yield select();
    const cart = state.data.cart;
    const breakfast = state.data.breakfast;

    const item = cart.filter(item => item.product_id === action.id);
    // switch case by action.category
    switch (action.category) {
      case 'breakfast': {
        const alreadyIn = breakfast.filter(
          item => item.product_id === action.id
        );
        if (alreadyIn.length) {
          console.log('in');
        } else {
          yield put({ type: ADD_TO_BREAKFAST, item: item[0] });
        }
      }
    }
  }
}
