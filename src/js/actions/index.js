import {
  ADD_VIEWED,
  ADD_TO_CART,
  GET_DATA,
  GET_PRODUCT,
  NEW_VALUE_AT_CART,
  REMOVE_FROM_CART,
  TOGGLE_HEADER_MENU,
  TOGGLE_LOGIN_MENU,
  SIGN_IN_REQUEST,
  SIGN_UP_REQUEST,
  CLEAR_LOGIN_FORM_ERROR,
  REDIRECT_TO_PERSONAL_ACCOUNT,
  ADD_TO_FOOD_MENU,
} from '../constants';

export const addToFoodMenu = (id, category) => ({
  type: ADD_TO_FOOD_MENU,
  id,
  category,
});
export const getData = (category, page) => ({ type: GET_DATA, category, page });
export const addViewed = id => ({ type: ADD_VIEWED, id });
export const addToCart = product => ({ type: ADD_TO_CART, product });
export const newValueAtCart = val => ({ type: NEW_VALUE_AT_CART, val });
export const removeFromCart = (id, calories) => ({
  type: REMOVE_FROM_CART,
  id,
  calories,
});
export const getProduct = id => ({ type: GET_PRODUCT, id });
export const toggleLoginMenu = () => ({ type: TOGGLE_LOGIN_MENU });
export const toggleHeaderMenu = () => ({ type: TOGGLE_HEADER_MENU });
export const signIn = values => ({ type: SIGN_IN_REQUEST, values });
export const signUp = values => ({ type: SIGN_UP_REQUEST, values });
export const clearLoginFormError = () => ({ type: CLEAR_LOGIN_FORM_ERROR });
export const redirectToPersonalAccount = () => ({
  type: REDIRECT_TO_PERSONAL_ACCOUNT,
});
