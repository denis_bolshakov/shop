import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import CartInfo from '../../components/cartinfo';

describe('CartInfo', () => {
  it('should render correctly', () => {
    const output = shallow(<CartInfo cart={[]} calories={0} />);
    expect(shallowToJson(output)).toMatchSnapshot();
  });
});
