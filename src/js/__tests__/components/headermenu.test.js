import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import HeaderMenu from '../../components/headerMenu';

describe('HeaderMenu', () => {
  const mockFn = jest.fn();
  const props = {
    cart: [],
    calories: 0,
    cartLength: 5,
    actions: {
      toggleHeaderMenu: mockFn,
      toggleLoginMenu: mockFn,
    },
  };
  const output = shallow(<HeaderMenu {...props} />);
  it('should render correctly', () => {
    expect(shallowToJson(output)).toMatchSnapshot();
  });
  it('it should call action when button have been clicked', () => {
    output.find('button').simulate('click');
    expect(props.actions.toggleHeaderMenu).toHaveBeenCalled();
  });
  it('it should call action when login-container clicked', () => {
    output.find('.login-container').simulate('click');
    expect(props.actions.toggleLoginMenu).toHaveBeenCalled();
  });
});
