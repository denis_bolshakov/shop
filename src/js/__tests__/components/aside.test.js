import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import Aside from '../../components/aside';

describe('Aside', () => {
  it('should render correctly', () => {
    const output = shallow(<Aside isHeaderMenuOpen />);
    expect(shallowToJson(output)).toMatchSnapshot();
  });
});
