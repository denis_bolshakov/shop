import firebase from 'firebase';
import { push } from 'react-router-redux';
import { call, put, take } from 'redux-saga/effects';
import { signInSaga } from '../../sagas/auth';

import {
  SIGN_IN_REQUEST,
  LOGIN_FORM_SUBMITTING,
  LOGIN_FORM_SUCCESS,
  LOGIN_FORM_ERROR,
  TOGGLE_LOGIN_MENU,
} from '../../constants';

const saga = signInSaga();
const requestAction = {
  type: SIGN_IN_REQUEST,
  values: {
    email: 'admin@admin.ru',
    password: '12341234',
  },
};

const user = {
  email: requestAction.values.email,
};

describe('sign in saga testing', () => {
  it('should take sign in request', () => {
    expect(saga.next().value).toEqual(take(SIGN_IN_REQUEST));
  });
  it('should make login form submitting request', () => {
    expect(saga.next().value).toEqual(put({ type: LOGIN_FORM_SUBMITTING }));
  });
  it('should call', () => {
    expect(JSON.stringify(saga.next(requestAction).value)).toEqual(
      JSON.stringify(
        call(() =>
          firebase
            .auth()
            .signInWithEmailAndPassword(
              requestAction.values.email,
              requestAction.values.password
            )
        )
      )
    );
  });
  it('should make success request with user data', () => {
    expect(saga.next(user).value).toEqual(
      put({
        type: LOGIN_FORM_SUCCESS,
        payload: user,
      })
    );
  });

  it('should make toggle menu action ', () => {
    expect(saga.next().value).toEqual(put({ type: TOGGLE_LOGIN_MENU }));
  });

  it('should push to personal account', () => {
    expect(saga.next().value).toEqual(put(push('/account')));
  });

  it('should make error action ', () => {
    const error = new Error('error');
    expect(saga.throw(error).value).toEqual(
      put({
        type: LOGIN_FORM_ERROR,
        error,
      })
    );
  });
});
