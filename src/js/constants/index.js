export const GET_PRODUCT = 'GET_PRODUCT';
export const GET_DATA = 'GET_DATA';
export const PRODUCT_SUCCESS = 'PRODUCT_SUCCESS';
export const DATA_SUCCESS = 'DATA_SUCCESS';
export const DATA_PENDING = 'DATA_PENDING';
export const DATA_ERROR = 'DATA_ERROR';
export const ADD_TO_CART = 'ADD_TO_CART';
export const ADD_VIEWED = 'ADD_VIEWED';
export const NEW_VALUE_AT_CART = 'NEW_VALUE_AT_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const TOGGLE_HEADER_MENU = 'TOGGLE_HEADER_MENU';
export const TOGGLE_LOGIN_MENU = 'TOGGLE_LOGIN_MENU';

export const SIGN_IN_REQUEST = 'SIGN_IN_REQUEST';
export const SIGN_UP_REQUEST = 'SIGN_UP_REQUEST';
export const LOGIN_FORM_SUBMITTING = 'LOGIN_FORM_SUBMITTING';
export const LOGIN_FORM_SUCCESS = 'LOGIN_FORM_SUCCESS';
export const LOGIN_FORM_ERROR = 'LOGIN_FORM_ERROR';
export const CLEAR_LOGIN_FORM_ERROR = 'CLEAR_LOGIN_FORM_ERROR';
export const REDIRECT_TO_PERSONAL_ACCOUNT = 'REDIRECT_TO_PERSONAL_ACCOUNT';
export const ADD_TO_FOOD_MENU = 'ADD_TO_FOOD_MENU';
export const ADD_TO_BREAKFAST = 'ADD_TO_BREAKFAST';
