﻿import React from 'react';
import ReactDOM from 'react-dom';
import { ConnectedRouter, routerMiddleware } from 'react-router-redux';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import createHistory from 'history/createBrowserHistory';
import App from './components/app';
import reducer from './reducers';
import logger from './middlewares/logger';
import '../style/index';
import './firebase/firebase';
import sagas from './sagas';

const sagaMiddleware = createSagaMiddleware();
const history = createHistory();
const store = createStore(
  reducer,
  composeWithDevTools(
    applyMiddleware(logger, sagaMiddleware, routerMiddleware(history))
  )
);
sagaMiddleware.run(sagas);

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('app')
);
