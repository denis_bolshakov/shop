# Calories calculator

This is a test project that allows you to add products, while doing the calculation of calories.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Installing

clone repository

npm install (Make sure you have Node and NPM installed)

npm start

## Running the tests

npm test

## Built With

- [Node.js](https://nodejs.org/) - JavaScript runtime built on Chrome's V8 JavaScript engine.
- [express](https://expressjs.com/) - Fast, unopinionated, minimalist web framework for Node.js.
- [firebase](https://firebase.google.com/) - Google's mobile platform that helps you quickly develop high-quality apps.
- [react](https://reactjs.org/) - A JavaScript library for building user interfaces.
- [react-dnd](http://react-dnd.github.io) -Drag and Drop for React.
- [react-router](https://reacttraining.com/react-router/core) - Declarative Routing for React.js.
- [redux](https://redux.js.org/) - Predictable state container for JavaScript apps.
- [redux-form](https://redux-form.com/) - Manage form state in Redux.
- [redux-saga](https://github.com/redux-saga/redux-saga) - An alternative side effect model for Redux apps.
- [jest](https://jestjs.io/) - Delightful JavaScript Testing.
- [enzyme](https://github.com/airbnb/enzyme) - Testing utilities for React.
- [less](http://lesscss.org/) - Backwards-compatible language extension for CSS.
- [webpack](https://webpack.js.org/) - Module bundler.

## Authors

- **Denis Bolshakov** - _Initial work_ - (http://basicweb.ru)

## License

This project is licensed under the MIT License
