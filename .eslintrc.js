module.exports = {
  extends: 'airbnb',
  plugins: ['react', 'prettier'],
  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 6,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
  },
  rules: {
    'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
    'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
    'comma-dangle': ['error', 'always-multiline'],
    'object-curly-newline': ['error', { consistent: true }],
    'no-plusplus': 'off',
    'prettier/prettier': 'error',
    strict: 0,
    'linebreak-style': ['error', 'windows'],
    'arrow-parens': [2, 'as-needed'],
    indent: ['warn', 2, { SwitchCase: 1 }],
    'react/jsx-indent': ['warn', 2],
    'jsx-a11y/label-has-for': [
      2,
      {
        components: ['Label'],
        required: {
          some: ['nesting', 'id'],
        },
        allowChildren: false,
      },
    ],
  },
  env: {
    browser: true,
  },
};
