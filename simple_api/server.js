/* const {MongoClient} = require("mongodb");
const url = 'mongodb://localhost:27017';
const dbName = 'timeSeries';

const mongoClient = new MongoClient(url);

mongoClient.connect(function(err, client) {
		const db = client.db(dbName);
		const collection = db.collection("OrderBooks");
		console.log(collection);
    client.close();
  }); */

var express = require('express');
var data = require("./goods");
var app = express();
var bodyParser = require('body-parser');


app.set('port', process.env.PORT || 3001);

// create application/json parser
var jsonParser = bodyParser.json()
 
// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

app.get('/api/home', (req, res) => {
   const page = req.query.page,
         itemsPerPage = 5;

    let totalPages = data.length;
    let end = page * itemsPerPage;
    let start = end - itemsPerPage;

    if (page < 1 || page > totalPages) {
        return;
    }

    let items = {
       data: data.slice(start, end),
       items: totalPages,
       itemsPerPage: itemsPerPage
    }
     res.send(items);
});

app.post('/api/add', jsonParser, (req, res) => {
 console.log(req.body);
 res.status(200).json({status:"ok"});
});

app.get('/api/product/:id', (req, res) => {
    const currentId = req.params.id;

    let item = data.find(function (currentValue) {
        return currentValue.product_id == currentId;
    });

    res.send([item]);
});
app.get('/api/fruits', (req, res) => {
    let fruits = data.filter(function (item) {
        return item.categories.includes("fruits");
    });

    const page = req.query.page,
          itemsPerPage = 5;

    let totalPages = fruits.length;
    let end = page * itemsPerPage;
    let start = end - itemsPerPage;

    if (page < 1 || page > totalPages) {
        return;
    }

    let items = {
        data: fruits.slice(start, end),
        items: totalPages,
        itemsPerPage: itemsPerPage
    }
    res.send(items);

});

app.get('/api/vegetables', (req, res) => {
    let vegetables = data.filter(function (item) {
        return item.categories.includes("vegetables");
    });

    const page = req.query.page,
        itemsPerPage = 5;

    let totalPages = vegetables.length;
    let end = page * itemsPerPage;
    let start = end - itemsPerPage;

    if (page < 1 || page > totalPages) {
        return;
    }

    let items = {
        data: vegetables.slice(start, end),
        items: totalPages,
        itemsPerPage: itemsPerPage
    }
    res.send(items);
});


app.get('/api/beef', (req, res) => {
    let beef = data.filter(function (item) {
        return item.categories.includes("beef");
    });
    let items = {
        data: beef.slice(0,5),
        items: beef.length,
        itemsPerPage: 5
    }
    res.send(items);
});

app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port') + '; press Ctrl-C to terminate.');
});